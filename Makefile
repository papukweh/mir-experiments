setup:
	download_beatmaps
	install-node-deps
	install-python-deps

download-beatmaps:
	./download_beatmaps.sh

install-node-deps:
	(cd osu-parser; yarn install)

install-python-deps:
	conda env create -f environment.yml
	conda activate FMP

parse:
	node osu-parser/parse

jupyter:
	jupyter notebook

clean:
	rm experiment-files/audio/*
	rm experiment-files/osu/*
	rm experiment-files/notes/*
	rm experiment-files/osu-files/*